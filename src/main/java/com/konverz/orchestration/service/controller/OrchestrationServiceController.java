package com.konverz.orchestration.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.konverz.orchestration.service.data.entity.User;
import com.konverz.orchestration.service.domain.OrchestrationService;
import com.konverz.orchestration.service.dto.UserMessage;

@RestController
@RequestMapping(value = "/orchestration", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrchestrationServiceController {

	@Autowired
	private OrchestrationService service;
	
	@RequestMapping(value = "/reply")
	private String replyOnMessage(@RequestBody final UserMessage message) throws Exception {
		return service.replyOnMessage(message);
	}

	@RequestMapping(value = "/debug_set_user")
	private User updateUser(@RequestBody final User user) throws Exception {
		return service.updateUser(user);
	}
	
}
