package com.konverz.orchestration.service.domain.impl;

import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konverz.orchestration.service.data.entity.User;
import com.konverz.orchestration.service.data.repository.UserRepository;
import com.konverz.orchestration.service.domain.OrchestrationService;
import com.konverz.orchestration.service.dto.NotificationMessage;
import com.konverz.orchestration.service.dto.UserMessage;
import com.konverz.orchestration.service.utils.ServiceCaller;

@Service
public class DefaultOrchestrationService implements OrchestrationService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Value("${com.konverz.service-discovery.nlp-service-url}")
	private String nlpEngineUrl;
	
	@Value("${com.konverz.service-discovery.human-interaction-service-url}")
	private String humanInteractionServiceUrl;
	
	@Autowired
	private ServiceCaller caller;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ObjectMapper mapper;
	
	public String replyOnMessage(UserMessage message) throws Exception {
		User user = userRepository.findById(message.getUserId());
		
		if (user == null)
			return ""; //do nothing
		
		try {	
			String param = UriUtils.encode(message.getText(), "UTF-8");
			
			JsonNode messageSemantics = 
					caller.get(nlpEngineUrl + "/witai_demo?message=" + param);
			
			JsonNode intent = messageSemantics.get("entities").get("intent");
			
			if (intent.isArray() && intent.size() > 0) {
				JsonNode firstIntent = intent.get(0);
				String intentValue = firstIntent.get("value").asText();
				
				if (intentValue.equals("tasks_today")) {
					StringBuilder response = new StringBuilder();
					
					if (user.getEvents().size() == 0)
						response.append("You have no events scheduled for today!");
					else {
						response.append("Hey " + user.getName() + ", you have the following events scheduled.");
						for (User.UserEvent event : user.getEvents())
							response.append("At " + event.getDate().getHours() + ":" + 
									event.getDate().getMinutes() + ", " + event.getTitle());
					}
					
					sendNotification(response.toString(), user);
				}
			}
			else
				throw new Exception("Intent is invalid! Message:" + message.getText());
				
		}
		
		catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			sendNotification("Sorry I am sick today. Please try another query.", user);
		}
		
		return "";
	}

	public User updateUser(User user) throws Exception {
		userRepository.save(user);
		return userRepository.findById(user.getUserId());
	}

	
	private void sendNotification(String message, User user) throws RestClientException, JsonProcessingException, URISyntaxException {
		NotificationMessage m = new NotificationMessage();
		m.setText(message);
		caller.post(
				humanInteractionServiceUrl + "/notification?webhookId=" + user.getIncomingWebhookId(), 
				mapper.writeValueAsString(m));
	}
}
