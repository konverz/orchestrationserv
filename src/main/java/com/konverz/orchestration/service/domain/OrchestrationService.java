package com.konverz.orchestration.service.domain;

import org.springframework.stereotype.Service;

import com.konverz.orchestration.service.data.entity.User;
import com.konverz.orchestration.service.dto.UserMessage;

@Service
public interface OrchestrationService {

	String replyOnMessage(UserMessage message) throws Exception;
	
	User updateUser(User user) throws Exception;
	
}
