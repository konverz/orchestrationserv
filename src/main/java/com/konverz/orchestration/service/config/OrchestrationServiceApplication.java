package com.konverz.orchestration.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.konverz.orchestration.service.data.entity.User;

@SpringBootApplication
@ComponentScan({
		"com.konverz.orchestration.service.config",
		"com.konverz.orchestration.service.controller",
		"com.konverz.orchestration.service.domain",
		"com.konverz.orchestration.service.data",
		"com.konverz.orchestration.service.utils",
})
@Configuration
@Import({
	WebConfig.class
})
public class OrchestrationServiceApplication {

	@Value("${com.konverz.datastore.host}")
	private String host;
	
	@Value("${com.konverz.datastore.username}")
	private String userName;
	
	@Value("${com.konverz.datastore.password}")
	private String password;
	
	public static void main(String args[]) {
		SpringApplication.run(OrchestrationServiceApplication.class, args);
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
		jedisConFactory.setHostName(host);
		jedisConFactory.setPort(6379);
		jedisConFactory.setClientName(userName);
		jedisConFactory.setPassword(password);
		
		return jedisConFactory;
	}
	 
	@Bean
	public RedisTemplate<String, User> redisTemplate() {
	    RedisTemplate<String, User> template = new RedisTemplate<String, User>();
	    template.setConnectionFactory(jedisConnectionFactory());
	    return template;
	}
	
}
