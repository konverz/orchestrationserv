package com.konverz.orchestration.service.utils;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ServiceCaller {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ObjectMapper mapper;
	
	public ResponseEntity<JsonNode> post(String url, String body) throws RestClientException, URISyntaxException {
		MultiValueMap<String, String> h = new LinkedMultiValueMap<String, String>();
		h.add("Content-Type", "application/json");
		HttpEntity<String> request = new HttpEntity<String>(body, h);
		
		return restTemplate.postForEntity(new URI(url), request, JsonNode.class);
	}
	
	public JsonNode get(String url) throws Exception {
		ResponseEntity<String> response = restTemplate.getForEntity(new URI(url), String.class);
		return mapper.readTree(response.getBody());
	}
}
