package com.konverz.orchestration.service.data.repository;

import org.springframework.stereotype.Repository;

import com.konverz.orchestration.service.data.entity.User;

@Repository
public interface UserRepository {

	User findById(String userId);
	
	void save(User user);
	
}
