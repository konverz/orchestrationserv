package com.konverz.orchestration.service.data.repository.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.konverz.orchestration.service.data.entity.User;
import com.konverz.orchestration.service.data.repository.UserRepository;

@Repository
public class RedisUserRepository implements UserRepository {

	private static final String KEY = "USER";
	
	@Autowired
	private RedisTemplate<String, User> redisTemplate;

	private HashOperations<String, String, User> hashOps;
	 
	@PostConstruct
	public void init() {
		hashOps = redisTemplate.opsForHash();
	}
	
	public User findById(String userId) {
		return (User) hashOps.get(KEY, userId);
	}

	public void save(User user) {
		hashOps.put(KEY, user.getUserId(), user);
	}

}
