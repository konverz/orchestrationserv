package com.konverz.orchestration.service.data.entity;

import java.io.Serializable;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonFormat;

public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userId;
	
	private String userToken;
	
	private String incomingWebhookId;
	
	private String outgoingWebhookId;
	
	private String name;
	
	private List<UserEvent> events = new ArrayList<UserEvent>();
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	public String getIncomingWebhookId() {
		return incomingWebhookId;
	}

	public void setIncomingWebhookId(String incomingWebhookId) {
		this.incomingWebhookId = incomingWebhookId;
	}

	public String getOutgoingWebhookId() {
		return outgoingWebhookId;
	}

	public void setOutgoingWebhookId(String outgoingWebhookId) {
		this.outgoingWebhookId = outgoingWebhookId;
	}

	public List<UserEvent> getEvents() {
		return events;
	}

	public void setEvents(List<UserEvent> events) {
		this.events = events;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static class UserEvent implements Serializable {
		
		private static final long serialVersionUID = 1L;

		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
		private Date date;
		
		private String title;

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
		
	}
	
}
