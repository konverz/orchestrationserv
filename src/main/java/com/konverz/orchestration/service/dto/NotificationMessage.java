package com.konverz.orchestration.service.dto;

public class NotificationMessage {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
